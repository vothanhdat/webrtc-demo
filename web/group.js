
//@ts-check

let groupIDEl = document.getElementById('call-id');
let groupCallEl = document.getElementById('group-id');
let incomingCall = document.getElementById('incoming-call');



class CallGroup {
    constructor(id, userId) {
        this.id = id
        this.userId = userId
        this.token = token
        this.ws = new WebSocket(`wss://signals-dot-livestreaming-241004.appspot.com/group/?id=${id}`)
        this.ws.onmessage = (event) => {
            let [callerId, data] = JSON.parse(event.data)
            if (callerId == id)
                this.onInternalMessage(data)
            else
                this.onSocketMessage(callerId, data)
        }
    }

    onInternalMessage(data) {
        if (data && data.taken && data.takenId) {
            return this.onCancelCall(data.takenId)
        }
    }

    onSocketMessage(callerId, data) {
        if (data && data.request) {
            switch (data.request) {
                case "call":
                    return this.onIncomingCall(callerId)
                case "cancel-call":
                    return this.onCancelCall(callerId)
            }
        } else if (data && data.status) {
            switch (data.status) {
                case "disconnect":
                    return this.onCancelCall(callerId)
            }
        }

    }

    onIncomingCall(id) {
        try {
            document.getElementById(`call-btn-${id}`).remove()
        } catch (error) {}
        let btn = document.createElement("button")
        btn.id = `call-btn-${id}`
        btn.onclick = () => this.onAcceptCall(id)
        btn.innerText = `Call from ${id}`
        incomingCall.appendChild(btn)
    }

    onCancelCall(id) {
        try {
            document.getElementById(`call-btn-${id}`).remove()
        } catch (error) { }
    }


    onAcceptCall(id) {
        this.ws.send(JSON.stringify([id, { response: "accepted", callId: this.userId, token: this.token }]))
        this.ws.send(JSON.stringify([this.id, { taken: true, takenId: id }]))
        try {
            document.getElementById(`call-btn-${id}`).remove()
        } catch (error) { }
    }

    onRejectCall(id) {
        this.ws.send(JSON.stringify([id, { response: "rejected" }]))
        try {
            document.getElementById(`call-btn-${id}`).remove()
        } catch (error) { }
    }
}

function startGroup() {
    new CallGroup(groupCallEl.value, id)
}


async function callToGroup() {
    let myId = id
    let groupId = groupIDEl.value

    let ws = new WebSocket(`ws://localhost:8080/call-group/?id=${myId}`)

    let ev = new EventEmitter()

    ws.onopen = () => {
        ws.send(JSON.stringify([groupId, { request: "call" }]))
    }


    ws.onmessage = msg => {
        let [gpId, data] = JSON.parse(event.data)
        if (gpId != groupId)
            return

        if (data.response) {
            ev.emit("response", data)
        } else if (data.status) {
            ev.emit("status", data)
        }
    }

    let res = await new Promise(rs => {
        ev.once("response",rs)
    })

    if(res.response == "accepted"){
        await callrtc.start(res.callId,res.token)
        .catch(e => alert(JSON.stringify(e)))
    }

}
